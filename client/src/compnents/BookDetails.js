import React, { Component } from "react";
import { graphql } from "react-apollo";
import { getBookQuery } from "../queries/quieries";

class BookDetails extends Component {
  displayBookDetails() {
    const { book } = this.props.data;
    if (book) {
      return (
        <div>
          <h2>{book.name}</h2>
          <p>{book.genre}</p>
          <p>{book.author.name}</p>
          <ul class="others-books">
            {book.author.books.map(book => {
              return (
                <li key={book.id}>{book.name}</li>
              )
            })}
          </ul>
        </div>
      );
    } else {
      return <div>No book selected</div>
    }
  }
  render() {
    console.log(this.props);
    return (
      <div id="book-details">
        <h3>Book details</h3>
        {this.displayBookDetails()}
      </div>
    );
  }
}
export default graphql(getBookQuery, {
  options: props => {
    return {
      variables: {
        id: props.bookId
      }
    };
  }
})(BookDetails);
